# Thème API Wordpress | Molotov

## Préparation

- `composer install`

## Développement

À chaque commit, des hooks de validation sont exécutés pour s'assurer que certaines
conventions stylisques et structurelles des thèmes Wordpress sont bien suivies.

Commandes utiles:

* Pour contourner les hooks: `git commit --no-verify`
* Pour corriger automatiquement plusieurs problèmes: `vendor/bin/phpcbf .`
* Pour valider un ou des fichiers: `vendor/bin/phpcs CHEMIN`
