<?php

if ( file_exists( 'vendor/autoload.php' ) ) {
	require( 'vendor/autoload.php' );
}

require( 'lib/blank.php' );
require( 'lib/helpers.php' );

Molotov\Setup::setup();
