<?php

if ( ! function_exists( 'mb_ucfirst' ) ) {
	/**
	 * Multibyte compliant ucfirst.
	 */
	function mb_ucfirst( $string, $encoding = 'utf-8' ) {
		$strlen     = mb_strlen( $string, $encoding );
		$first_char = mb_substr( $string, 0, 1, $encoding );
		$then       = mb_substr( $string, 1, $strlen - 1, $encoding );
		return mb_strtoupper( $first_char, $encoding ) . $then;
	}
}

if ( ! function_exists( 'dig' ) ) {
	/**
	 * Get a nested key in an array, "dig" in the array.
	 */
	function dig( $target, $key, $default = null ) {
		$parts = explode( '.', $key, 2 );

		if ( count( $parts ) === 1 ) {
			if ( isset( $target[ $key ] ) && $target[ $key ] !== null ) {
				return $target[ $key ];
			}
		}

		$key  = $parts[0];
		$rest = isset( $parts[1] ) ? $parts[1] : '';

		if ( isset( $target[ $key ] ) && $target[ $key ] !== null ) {
			return dig( $target[ $key ], $rest, $default );
		}

		return $default;
	}
}

if ( ! function_exists( 'remove_rest_base_from_internal_acf_links' ) ) {
	function remove_rest_base_from_internal_acf_links( $link_field ) {
		if ( ! $link_field ) {
			return null;
		}

		$link_field['url'] = strip_home_url( dig( $link_field, 'url' ) );

		return $link_field;
	}
}

if ( ! function_exists( 'strip_home_url' ) ) {
	function strip_home_url( $permalink ) {
		return str_replace( home_url(), '', $permalink );
	}
}

if ( ! function_exists( 'molotov_build_subnavigation' ) ) {
	function molotov_build_subnavigation( &$response ) {
		$subnavigation = array();

		$matches = array();
		preg_match_all( '/<h2>(.+)<\/h2>/', $response->data['content'], $matches );

		foreach ( $matches[0] as $index => $match ) {
			$name = $matches[1][ $index ];
			$id   = strtolower( trim( preg_replace( '/[^A-Za-z0-9-]+/', '-', $name ) ) );

			$response->data['content'] = str_replace(
				$match,
				'<a class="subnavigation-anchor" id="' . $id . '"></a>' .
				'<h2 class="section-title">' . $name . '</h2>',
				$response->data['content']
			);

			$subnavigation[ $id ] = $name;
		}

		if ( empty( $subnavigation ) ) {
			return new stdClass;
		}

		return $subnavigation;
	}
}
