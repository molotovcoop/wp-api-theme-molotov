<?php

namespace Molotov\PostTypes;

abstract class PostType {

	public static $name;

	public static function init() {
		if ( ! static::$name ) {
			throw \Exception( 'Provide a name for this post type' );
		}

		add_action( 'init', array( static::class, 'register' ) );

		$name = static::$name;
		add_filter( "rest_after_insert_$name", array( static::class, 'set_terms' ), 10, 2 );
		add_filter( "rest_pre_insert_$name", array( static::class, 'add_permalink' ), 19, 2 );
	}

	abstract public static function register();

	public static function set_terms( $post, $request ) {
		$params = $request->get_json_params();
		if ( array_key_exists( 'terms', $params ) ) {
			foreach ( $params['terms'] as $taxonomy => $terms ) {
				wp_set_post_terms( $post->ID, $terms, $taxonomy );
			}
		}
	}

	public static function add_permalink( $post, $request ) {
		$post->meta_input['drupal_permalink'] = $request->get_param( 'drupal_permalink' );
		return $post;
	}
}
