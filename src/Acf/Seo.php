<?php

acf_add_local_field_group(
	array(
		'key'                   => 'group_5c9e7a1c353c3',
		'title'                 => 'SEO',
		'fields'                => array(
			array(
				'key'               => 'field_5c9e7a4f5995d',
				'label'             => 'Titre',
				'name'              => 'og_title',
				'type'              => 'text',
				'instructions'      => "Titre pour le partage sur les réseaux sociaux. Laisser vide pour utiliser le titre de la page ou de l'article.",
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5c9e7a7d5995e',
				'label'             => 'Description',
				'name'              => 'og_description',
				'type'              => 'textarea',
				'instructions'      => 'Court texte utilisé pour le partage sur les réseaux sociaux. Laisser vide pour utiliser les premières phrases du contenu.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => '',
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5c9e7a2059958',
				'label'             => 'Image',
				'name'              => 'og_image',
				'type'              => 'image',
				'instructions'      => 'Image pour le partage sur les réseaux sociaux. Laisser vide pour utiliser l\'image mise en avant ou, si n\'y en a pas, l\'image par défaut configurée dans les options du thème.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'fit-width',
				'library'           => 'all',
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'post',
				),
			),
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'page',
				),
			),
		),
		'menu_order'            => 10,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => 'Pour tous les contenus',
	)
);

acf_add_local_field_group(
	array(
		'key'                   => 'group_5c9e7ba71da1c',
		'title'                 => 'SEO',
		'fields'                => array(
			array(
				'key'               => 'field_5c9e7bbcaf791',
				'label'             => 'Image',
				'name'              => 'og_image',
				'type'              => 'image',
				'instructions'      => 'Image par défaut pour le partage sur les réseaux sociaux.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'id',
				'preview_size'      => 'fit-width',
				'library'           => 'all',
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order'            => 9,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => 'Options générales',
	)
);
