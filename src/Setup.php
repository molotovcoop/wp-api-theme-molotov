<?php

namespace Molotov;

class Setup {

	public static function setup() {
		add_action( 'after_setup_theme', array( 'Molotov\Setup', 'enable_theme_features' ) );
		add_action( 'after_setup_theme', array( 'Molotov\Setup', 'register_menus' ) );

		add_action( 'rest_api_init', array( 'Molotov\Setup', 'register_controllers' ), 20 );

		self::setup_tiny_mce();
		self::add_image_sizes();
		self::configure_acf();

		add_filter(
			'jwt_auth_default_whitelist',
			function ( $default_whitelist ) {
				return array_merge(
					$default_whitelist,
					array_map(
						function ( $p ) {
							return '/wp' . $p;
						},
						$default_whitelist
					),
					array( '/wp-json/jwt-auth/', '/wp/wp-json/molotov/v1/' )
				);
			}
		);

		self::register_custom_post_types();
	}

	public static function setup_tiny_mce() {
		add_editor_style( 'editor.css' );

		add_filter(
			'mce_buttons_2',
			function ( $buttons ) {
				array_unshift( $buttons, 'styleselect' );
				return $buttons;
			}
		);

		add_filter(
			'tiny_mce_before_init',
			function ( $array ) {
				$formats                = array();
				$array['style_formats'] = json_encode( $formats );

				return $array;
			}
		);
	}

	public static function enable_theme_features() {
		add_theme_support( 'menus' );
		add_theme_support( 'post-thumbnails' );

		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Noir', 'molotov' ),
					'slug'  => 'molotov-black',
					'color' => '#000000',
				),
				array(
					'name'  => __( 'Blanc', 'molotov' ),
					'slug'  => 'molotov-white',
					'color' => '#ffffff',
				),
			)
		);
	}

	public static function register_menus() {
		register_nav_menu( 'primary', __( 'Main Menu', 'molotov' ) );
		register_nav_menu( 'footer', __( 'Footer Menu', 'molotov' ) );
	}

	public static function add_image_sizes() {
		add_image_size( 'og', 1200, 630, true );
		add_image_size( 'xlarge', 1920 );
	}

	public static function configure_acf() {
		if ( function_exists( 'acf_add_options_sub_page' ) ) {
			acf_add_options_sub_page(
				array(
					'page_title' => __( 'Options', 'molotov' ),
					'menu_title' => __( 'Options', 'molotov' ),
					'menu_slug'  => 'theme-general-settings',
					'capability' => 'edit_theme_options',
					'redirect'   => false,
				)
			);

			add_action(
				'acf/init',
				function () {
					acf_update_setting( 'google_api_key', '' );
				}
			);
		}

		// Import fields.
		foreach ( glob( __DIR__ . '/Acf/*.php' ) as $file ) {
			require_once( $file );
		}
	}

	public static function register_custom_post_types() {
	}

	public static function register_controllers() {
		foreach ( glob( __DIR__ . '/Controllers/*Controller.php' ) as $file ) {
			$base_class_name = str_replace( '.php', '', basename( $file ) );

			if ( 'Controller' === $base_class_name ) {
				continue;
			}

			$class_name = "Molotov\\Controllers\\$base_class_name";
			$class_name::get_instance();
		}
	}
}
