<?php

namespace Molotov\Controllers;

class Controller extends \WP_REST_Posts_Controller {

	protected $namespace = 'molotov/v1';
	protected $post_type;
	protected $meta;
	protected $rest_base;
	protected $routes;

	public static function get_instance() {
		return static::$instance ?: ( new static() );
	}

	protected function __construct() {
		static::$instance = $this;

		$this->register_routes();

		add_filter(
			"rest_prepare_{$this->post_type}",
			array( $this, 'rest_prepare_post_molotov' ),
			20,
			3
		);

		add_filter(
			"rest_prepare_{$this->post_type}",
			array( $this, 'rest_prepare_post_molotov_extract_rendered' ),
			30,
			3
		);

		add_filter(
			"rest_{$this->post_type}_query",
			array( $this, 'rest_prepare_post_molotov_query_taxonomies' ),
			20,
			2
		);

		$this->meta = new \WP_REST_Post_Meta_Fields( $this->post_type );
	}

	private static $slugs = array(
		404 => 'not_found',
	);

	private static $messages = array(
		'not_found' => 'Not found',
	);

	public function get_item_by_slug( $request ) {
		$request['id'] = dig( $request, 'id' )
			?: $this->get_post_id_by_slug( $request['slug'] );
		return $this->get_item( $request );
	}

	public function get_item_by_slug_permissions_check( $request ) {
		$request['id'] = dig( $request, 'id' )
			?: $this->get_post_id_by_slug( $request['slug'] );
		return $this->get_item_permissions_check( $request );
	}

	public function get_item_permissions_check( $request ) {
		if ( class_exists( \JWTAuth\Auth::class ) ) {
			$auth   = new \JWTAuth\Auth();
			$result = $auth->validate_token( false );

			if ( isset( $result->data->user->id ) ) {
				wp_set_current_user( $result->data->user->id );
			}
		}

		if ( function_exists( 'members_can_current_user_view_post' ) ) {
			if ( ! members_can_current_user_view_post( $request['id'] ) ) {
				return new \WP_Error(
					'rest_forbidden_context',
					__( 'Sorry, you are not allowed to view this post.' ),
					array( 'status' => rest_authorization_required_code() )
				);
			}
		}

		return parent::get_item_permissions_check( $request );
	}

	public function rest_prepare_post_molotov( $response, $post, $request ) {
		if ( strpos( $request->get_route(), '/molotov' ) !== 0 ) {
			return $response;
		}
		return $response;
	}

	public function rest_prepare_post_molotov_extract_rendered( $response, $post, $request ) {
		if ( ! is_admin() ) {
			foreach ( array( 'guid', 'title', 'content', 'excerpt' ) as $key ) {
				$value                  = dig( $response->data, $key );
				$response->data[ $key ] = dig( $value, 'rendered' );
			}
		}
		return $response;
	}

	protected function get_translations( $post ) {
		$locales  = array_keys( apply_filters( 'wpml_active_languages', array() ) );
		$home_url = home_url();

		return array_reduce(
			$locales,
			function ( $acc, $locale ) use ( $post, $home_url ) {
				$id = icl_object_id( $post['id'], 'page', false, $locale );
				if ( $id ) {
					$acc[] = array(
						'href'   => str_replace( $home_url, '', get_permalink( $id ) ),
						'locale' => $locale,
					);
				}
				return $acc;
			},
			array()
		);
	}

	protected function prepare_acf_fields( $acf_fields, &$item ) {
		if ( $acf_fields ) {
			foreach ( $acf_fields as $key => $field ) {
				$field_object = get_field_object( $key, $item['id'] );

				// Handle collection and recursive field types (one layer deep) first.
				switch ( $field_object['type'] ) {
					case 'repeater':
						$inner_fields = array();
						foreach ( $field_object['sub_fields'] as $key => $inner_field ) {
							if ( array_key_exists( $key, $field_object['value'] ) ) {
								$inner_field['value'] = $field_object['value'][ $key ];
								$inner_fields[ $key ] = $this->format_acf_field( $inner_field );
							}
						}
						$item[ $field_object['name'] ] = $inner_fields;
						break;
					case 'clone':
						foreach ( $field_object['sub_fields'] as $key => $inner_field ) {
							$inner_field['value']         = $field_object['value'][ $inner_field['name'] ];
							$item[ $inner_field['name'] ] = $this->format_acf_field( $inner_field );
						}
						break;
					default:
						$item[ $field_object['name'] ] = $this->format_acf_field( $field_object );
						break;
				}
			}
		}
	}

	protected function format_acf_field( $field_object ) {
		switch ( $field_object['type'] ) {
			case 'image':
				return $this->format_media( $field_object['value'] );
				break;
			case 'gallery':
				$gallery = array();
				if ( $field_object['value'] ) {
					foreach ( $field_object['value'] as $image_id ) {
						$image = $this->format_media( $image_id );
						array_push( $gallery, $image );
					}
				}
				return $gallery;
				break;
			case 'link':
				return remove_rest_base_from_internal_acf_links( $field_object['value'] );
				break;
			case 'file':
				return $field_object['value'];
				break;
			case 'text':
			case 'textarea':
			case 'email':
			case 'url':
			case 'date_picker':
			case 'time_picker':
			case 'date_time_picker':
			case 'color_picker':
			case 'true_false':
			case 'radio':
				return $field_object['value'];
				break;
			case 'repeater':
			case 'clone':
				return 'Unsupported 2 layers deep (or more) nested collection field. Implement it in prepare_acf_fields and format_acf_field.';
				break;
			default:
				return 'Unsupported acf field type. Declare it in prepare_acf_fields';
				break;
		}
	}

	protected function format_item( &$item ) {
		$acf_fields = get_fields( $item['id'] );
		$this->prepare_acf_fields( $acf_fields, $item );
	}

	protected function error( $code, $slug = null, $message = null ) {
		if ( ! $slug ) {
			$slug = dig( self::$slugs, $code, 'error' );
		}

		if ( ! $message ) {
			$message = dig( self::$messages, $slug, 'Error' );
		}

		return new \WP_Error( $slug, __( $message, 'molotov' ), array( 'status' => 404 ) );
	}

	public function register_routes() {
		foreach ( $this->routes as $url => $config ) {
			register_rest_route( $this->namespace, $url, $config );
		}
	}

	protected function format_media( $id, $properties = array() ) {
		$base = wp_get_attachment_metadata( $id );

		if ( ! $base ) {
			return null;
		}

		$base['url']     = wp_get_attachment_url( $id );
		$base['pattern'] = false;
		$base['alt']     = get_post_meta( $id, '_wp_attachment_image_alt', true );

		foreach ( $base['sizes'] as $format => &$size ) {
			$base['sizes'][ $format ]['url'] = wp_get_attachment_image_src( $id, $format )[0];
		}

		$sizes = wp_get_additional_image_sizes();
		foreach ( $sizes as $format => &$size ) {
			$base['sizes'][ $format ]['url'] = wp_get_attachment_image_src( $id, $format )[0];
		}

		return array_merge( $base, $properties );
	}

	protected function get_first_taxonomy_term( $post_id, $taxonomy_slug ) {
		$terms = get_the_terms( $post_id, $taxonomy_slug );
		if ( $terms ) {
			return $this->format_taxonomy( $terms[0] );
		}

		return '';
	}

	protected function format_taxonomy( $term ) {
		switch ( $term->taxonomy ) {
			case 'category':
				return array(
					'id'   => $term->term_id,
					'name' => $term->name,
					'slug' => $term->slug,
				);
				break;
			default:
				return array(
					'id'   => $term->term_id,
					'name' => $term->name,
					'slug' => $term->slug,
				);
		}
	}

	public function rest_prepare_post_molotov_query_taxonomies( $args, $request ) {

		// Use search by slug instead of the default term_id.
		if ( isset( $args['tax_query'] ) ) {
			$tax_query = $args['tax_query'];
			if ( isset( $tax_query ) ) {
				$tax_query[0]['field'] = 'slug';
				$args['tax_query']     = $tax_query;
			}
		}

		return $args;
	}

	protected function parse_hrefs( $content ) {
		$matches = array();
		$regex   = '|href="(' . get_site_url() . '.+)"|';
		preg_match_all( $regex, $content, $matches );

		foreach ( $matches[1] as $match ) {
			$url     = parse_url( $match );
			$content = str_replace( $match, $url['path'], $content );
		}

		return $content;
	}

	protected function rename_field( $from, $to, &$item ) {
		$item->data[ $to ] = $item->data[ $from ];
		unset( $item->data[ $from ] );
	}

	protected function get_post_id_by_slug( $slug ) {
		$item = get_page_by_path( $slug, OBJECT, $this->post_type );

		return $item ? $item->ID : null;
	}

	protected function build_permalink( $id ) {
		return str_replace( str_replace( '/wp', '', get_site_url() ), '', get_permalink( $id ) );
	}
}
