<?php

namespace Molotov\Controllers;

class SearchController extends Controller {

	protected static $instance;

	protected function __construct() {
		$this->rest_base     = 'search';
		$this->post_type     = 'post';
		$this->wp_controller = new \WP_REST_Posts_Controller( $this->post_type );

		$this->routes = array(
			"{$this->rest_base}/(?P<search>[a-zA-Z0-9 %_-]+)" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'getPosts' ),
				'permission_callback' => '__return_true',
			),
		);

		$this->register_routes();
	}

	public function getPosts( $request ) {
		$q = mb_strtolower( $request->get_param( 'search' ) ?: '' );

		$query         = new \WP_Query();
		$query_args    = array(
			'ep_integrate' => true,
			'post_type'    => 'any',
			's'            => $q,
		);
		$posts_results = $query->query( $query_args );

		$posts       = array_reduce(
			$posts_results,
			function ( $acc, $post ) use ( $request ) {
				if ( ! $this->wp_controller->check_read_permission( $post ) ) {
					return $acc;
				}

				$data  = $this->formatItem( $post, $request );
				$acc[] = $data->get_data();
				return $acc;
			},
			array()
		);
		$total_posts = $query->found_posts;

		if ( $total < 1 ) {
			// Out-of-bounds, run the query again without LIMIT for total count.
			unset( $query_args['paged'] );

			$count_query = new \WP_Query();
			$count_query->query( $query_args );
			$total_posts = $count_query->found_posts;
		}

		$response = rest_ensure_response( $posts );

		$response->header( 'X-WP-Total', (int) $total_posts );

		$response = $this->prepare_response( $response, $request );

		return $response;
	}
}
