<?php

namespace Molotov\Controllers;

class PostsController extends Controller {

	protected static $instance;

	protected function __construct() {
		self::$instance = $this;

		$this->rest_base     = 'posts';
		$this->post_type     = 'post';
		$this->wp_controller = new \WP_REST_Posts_Controller( $this->post_type );

		$this->routes = array(
			"/{$this->rest_base}"                          => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => '__return_true',
			),
			"/{$this->rest_base}/(?P<id>[0-9]+)"           => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item' ),
				'permission_callback' => '__return_true',
			),
			"/{$this->rest_base}/(?P<slug>[a-zA-Z0-9_-]+)" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item_by_slug' ),
				'permission_callback' => '__return_true',
			),
		);

		parent::__construct();
	}

	public function rest_prepare_post_molotov( $response, $post, $request ) {
		$this->format_item( $response->data );
		return $response;
	}

	protected function format_item( &$item ) {
		parent::format_item( $item );

		$item['translations'] = $this->get_translations( $item );
	}
}
