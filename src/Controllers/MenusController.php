<?php

namespace Molotov\Controllers;

class MenusController extends Controller {

	protected static $instance;

	protected function __construct() {
		$this->rest_base = 'menus';
		$this->routes    = array(
			"/{$this->rest_base}"                => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'getMenus' ),
				'permission_callback' => '__return_true',
			),
			"/{$this->rest_base}/(?P<id>[0-9]+)" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'getMenu' ),
				'permission_callback' => '__return_true',
				'args'                => array(
					'context' => array(
						'default' => 'view',
					),
				),
			),
			"/{$this->rest_base}-locations"      => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'getMenuLocations' ),
				'permission_callback' => '__return_true',
			),
			"/{$this->rest_base}-locations/(?P<location>[a-zA-Z0-9_-]+)" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'getMenuLocation' ),
				'permission_callback' => '__return_true',
			),
		);

		parent::__construct();
	}

	public function getMenus( $request ) {
		$rest_url = trailingslashit( get_rest_url() . $this->namespace . '/' . $this->rest_base );
		$wp_menus = wp_get_nav_menus();

		$rest_menus = array();
		foreach ( $wp_menus as $wp_menu ) {
			$menu = (array) $wp_menu;

			$rest_menus[ $menu['term_id'] ] = array(
				'id'          => (int) $menu['term_id'],
				'name'        => $menu['name'],
				'slug'        => $menu['slug'],
				'description' => $menu['description'],
				'count'       => $menu['count'],
				'meta'        => array(
					'collection' => $rest_url,
					'self'       => $rest_url . $menu['term_id'],
				),
			);
		};

		return apply_filters( 'rest_menus_format_menus', array_values( $rest_menus ) );
	}

	public function getMenu( $request ) {
		$id             = $request['id'];
		$rest_url       = get_rest_url() . $this->namespace . '/' . $this->rest_base;
		$wp_menu_object = $id ? wp_get_nav_menu_object( $id ) : false;
		$wp_menu_items  = $id ? wp_get_nav_menu_items( $id ) : false;

		$lang = apply_filters(
			'wpml_element_language_code',
			null,
			array(
				'element_id'   => $id,
				'element_type' => 'nav_menu',
			)
		);
		if ( ! $wp_menu_object ) {
			return $this->error( 404 );
		}

		if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
			define( 'ICL_LANGUAGE_CODE', null );
		}

		if ( ICL_LANGUAGE_CODE ) {
			$menu = (array) $wp_menu_object;

			$rest_menu_items = array();
			foreach ( $wp_menu_items as $item ) {
				$rest_menu_items[] = $this->formatmenu_item( $item );
			}
			$rest_menu_items = $this->nestmenu_items( $rest_menu_items, 0 );

			$rest_menu = array(
				'id'          => (int) $menu['term_id'],
				'name'        => $menu['name'],
				'slug'        => $menu['slug'],
				'description' => $menu['description'],
				'count'       => $menu['count'],
				'items'       => $rest_menu_items,
				'meta'        => array(
					'collection' => $rest_url,
					'self'       => $rest_url . $menu['term_id'],
				),
			);

			return apply_filters( 'rest_menus_format_menu', $rest_menu );
		}
		return $this->error( 404 );
	}

	public function getMenuLocations( $request ) {
		$locations        = get_nav_menu_locations();
		$registered_menus = get_registered_nav_menus();
		$rest_url         = get_rest_url() . $this->namespace . '/menu-locations/';

		$rest_menus = array();

		if ( $locations && $registered_menus ) {
			foreach ( $registered_menus as $slug => $label ) {
				if ( ! isset( $locations[ $slug ] ) ) {
					continue;
				}

				$rest_menus[ $slug ] = array(
					'id'    => $locations[ $slug ],
					'slug'  => $slug,
					'label' => $label,
					'meta'  => array(
						'collection' => $rest_url,
						'self'       => $rest_url . $slug,
					),
				);
			}
		}

		return array_values( $rest_menus );
	}

	public function getMenuLocation( $request ) {
		$params    = $request->get_params();
		$location  = $params['location'];
		$locations = get_nav_menu_locations();
		if ( ! isset( $locations[ $location ] ) ) {
			return array();
		}

		$wp_menu    = wp_get_nav_menu_object( $locations[ $location ] );
		$menu_items = wp_get_nav_menu_items( $wp_menu->term_id );

		$rev_items = array_reverse( $menu_items );
		$rev_menu  = array();
		$cache     = array();

		foreach ( $rev_items as $item ) {
			$formatted = $this->formatmenu_item( $item );

			if ( array_key_exists( $item->ID, $cache ) ) {
				$formatted['children'] = array_reverse( $cache[ $item->ID ] );
			}

			$formatted = apply_filters( 'rest_menus_format_menu_item', $formatted );

			if ( $item->menu_item_parent !== '0' ) {
				if ( array_key_exists( $item->menu_item_parent, $cache ) ) {
					array_push( $cache[ $item->menu_item_parent ], $formatted );
				} else {
					$cache[ $item->menu_item_parent ] = array( $formatted );
				}
			} else {
				array_push( $rev_menu, $formatted );
			}
		}
		return array_reverse( $rev_menu );
	}

	private function nestmenu_items( &$items, $parent = null ) {
		$parents  = array();
		$children = array();

		array_map(
			function ( $i ) use ( $parent, &$children, &$parents ) {
				if ( $i['id'] !== $parent && $i['parent'] === $parent ) {
					$parents[] = $i;
				} else {
					$children[] = $i;
				}
			},
			$items
		);

		foreach ( $parents as &$parent ) {
			if ( $this->hasChildren( $children, $parent['id'] ) ) {
				$parent['children'] = $this->nestmenu_items( $children, $parent['id'] );
			}
		}

		return $parents;
	}

	private function hasChildren( $items, $id ) {
		return array_filter(
			$items,
			function ( $i ) use ( $id ) {
				return $i['parent'] === $id;
			}
		);
	}

	private function formatmenu_item( $menu_item, $children = false, $menu = array() ) {
		$item = (array) $menu_item;

		$url_parts     = parse_url( dig( $item, 'url' ) );
		$path          = dig( $url_parts, 'path' );
		$new_menu_item = array(
			'id'          => (int) $item['ID'],
			'order'       => (int) $item['menu_order'],
			'parent'      => (int) $item['menu_item_parent'] ?: null,
			'title'       => $item['title'],
			'url'         => $item['url'],
			'path'        => $path,
			'attr'        => $item['attr_title'],
			'target'      => $item['target'],
			'classes'     => implode( ' ', $item['classes'] ),
			'xfn'         => $item['xfn'],
			'description' => $item['description'],
			'type'        => $item['type'],
			'children'    => array(),
		);

		if ( isset( $item['object_id'] ) ) {
			$pictogram = get_field( 'pictogram', $item['object_id'] );
			if ( $pictogram ) {
				$new_menu_item['pictogram'] = $this->format_media( $pictogram );
			}

			$color = get_field( 'section_color', $item['object_id'] );
			if ( $color ) {
				$new_menu_item['color'] = $color;
			}

			$tile = get_field( 'tile', $item['object_id'] );
			if ( $tile ) {
				$new_menu_item['tile'] = $this->format_media(
					$tile['id'],
					array(
						'pattern' => true,
					)
				);
			}
		}

		if ( $children === true && ! empty( $menu ) ) {
			$new_menu_item['children'] = $this->get_nav_menu_item_children( $item['ID'], $menu );
		}

		return apply_filters( 'rest_menus_format_menu_item', $new_menu_item );
	}
}
