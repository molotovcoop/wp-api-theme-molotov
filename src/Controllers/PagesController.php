<?php

namespace Molotov\Controllers;

class PagesController extends Controller {

	protected static $instance;

	protected function __construct() {
		$this->rest_base = '/pages';
		$this->post_type = 'page';

		$this->wp_controller = new \WP_REST_Posts_Controller( $this->post_type );

		$this->routes = array(
			"/{$this->rest_base}" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_collection' ),
				'permission_callback' => '__return_true',
			),
			"/{$this->rest_base}/(?P<slug>([a-zA-Z0-9_-]+\/?)+)" => array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item_by_slug' ),
				'permission_callback' => array(
					$this,
					'get_item_by_slug_permissions_check',
				),
			),
		);

		parent::__construct();
	}

	public function rest_prepare_post_molotov( $response, $post, $request ) {
		$this->format_item( $response->data );
		return $response;
	}

	protected function format_item( &$item ) {
		parent::format_item( $item );

		$item['template'] = $item['template'] ?: 'default';

		switch ( $item['template'] ) {
			case 'redirect':
				$item['target'] = get_field( 'target', $item['id'] ) === 'internal'
					? str_replace( get_site_url(), '', get_field( 'page', $item['id'] ) )
					: get_field( 'url', $item['id'] );
				break;
			default:
				break;
		}
	}
}
